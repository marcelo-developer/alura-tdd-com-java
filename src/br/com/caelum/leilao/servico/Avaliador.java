package br.com.caelum.leilao.servico;

import java.util.List;
import java.util.stream.Collectors;

import br.com.caelum.leilao.dominio.Lance;
import br.com.caelum.leilao.dominio.Leilao;

public class Avaliador {
	private double maiorDeTodos = Double.NEGATIVE_INFINITY;
	private double menorDeTodos = Double.POSITIVE_INFINITY;
	private double media = 0;
	List<Lance> maiores;
	
	public void avalia(Leilao leilao) {
		
		if (leilao.getLances().size() == 0)
			throw new RuntimeException("N�o � poss�vel avaliar um leil�o sem lances!");
		
		for (Lance lance: leilao.getLances()) {
			
			if (lance.getValor() > maiorDeTodos)
				maiorDeTodos = lance.getValor();
			if (lance.getValor() < menorDeTodos)
				menorDeTodos = lance.getValor();
			
			media += lance.getValor();
		}
		
		int size  = leilao.getLances().size();
		media /= ((size == 0) ? 1 : size);
		
		maiores = leilao.getLances().stream()
				.sorted((l1, l2) -> {
					return -Double.valueOf(l1.getValor()).compareTo(Double.valueOf(l2.getValor()));
				})
				.limit(3)
				.collect(Collectors.toList());
	}
	
	public List<Lance> getTresMaiores() {
		return maiores;
	}
	
	public double getMaiorLance() {
		return maiorDeTodos;
 	}
	
	public double getMenorLance() {
		return menorDeTodos;
	}
	
	public double getMediaDosLances() {
		return media;
	}
}
