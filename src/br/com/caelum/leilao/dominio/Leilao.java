package br.com.caelum.leilao.dominio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Leilao {

	private String descricao;
	private List<Lance> lances;
	
	public Leilao(String descricao) {
		this.descricao = descricao;
		this.lances = new ArrayList<Lance>();
	}
	
	public void propoe(Lance lanceAInserir) {
		
		if (lances.isEmpty() || podeDarlance(lanceAInserir.getUsuario()))
			lances.add(lanceAInserir);
	}

	public void dobraLance(Usuario usuario) {
		Lance ultimoLance = ultimoLanceDe(usuario);
		
		if (ultimoLance != null)
			propoe(new Lance(usuario,  ultimoLance.getValor()*2));
	}

	private Lance ultimoLanceDe(Usuario usuario) {
		Lance valor = null;
		List<Lance> lancesDoUsuario = lances.stream().filter(lance -> lance.getUsuario().equals(usuario)).collect(Collectors.toList());
		
		if (lancesDoUsuario.size() != 0)
			valor = lancesDoUsuario.get(lancesDoUsuario.size()-1);
		
		return valor;
	}
	
	private boolean podeDarlance(Usuario usuario) {
		int total = quantidadeDeLancesDe(usuario);
		return !ultimoLanceDado().getUsuario().equals(usuario) && total < 5;
	}

	private int quantidadeDeLancesDe(Usuario usuario) {
		int total = 0;
		
		for (Lance lanceCadastrado: lances) {
			
			if (lanceCadastrado.getUsuario().equals(usuario))
				total++;
		}
		return total;
	}

	public Lance ultimoLanceDado() {
		return lances.get(lances.size()-1);
	}

	public String getDescricao() {
		return descricao;
	}

	public List<Lance> getLances() {
		return Collections.unmodifiableList(lances);
	}

	
	
}
