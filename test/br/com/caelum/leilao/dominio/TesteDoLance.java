package br.com.caelum.leilao.dominio;

import org.junit.Test;

public class TesteDoLance {

	@Test(expected=IllegalArgumentException.class)
	public void naoDeveAceitarLanceMenorIgualAZero() {
		new Lance(new Usuario("teste"), -1);
	}
}
