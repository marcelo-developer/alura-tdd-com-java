package br.com.caelum.leilao.dominio;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

public class TesteDoLeilao {

	@Test
	public void deveReceberUmLance() {
		Leilao leilao = new Leilao("Macbook Pro 15");
		assertEquals(0, leilao.getLances().size());
		
		leilao.propoe(new Lance(new Usuario("Steve Jobs"), 2000));
		
		assertEquals(1, leilao.getLances().size());
		assertEquals(2000, leilao.getLances().get(0).getValor(), 0.00001);
	}
	
	@Test
	public void deveReceberVariosLances() {
		Leilao leilao = new Leilao("Macbook Pro 15");

		leilao.propoe(new Lance(new Usuario("Steve Jobs"), 2000));
		leilao.propoe(new Lance(new Usuario("Steve Wozniak"), 3000));

		assertEquals(2, leilao.getLances().size());
		assertEquals(2000, leilao.getLances().get(0).getValor(), 0.00001);
		assertEquals(3000, leilao.getLances().get(1).getValor(), 0.00001);
	}
	
	@Test
	public void naoDeveAceitarDoisLancesSeguidosDoMesmoUsuario() {
		Leilao leilao = new Leilao("Macbook Pro 15");
		Usuario usuario = new Usuario("Steve Jobs");
		
		leilao.propoe(new Lance(usuario, 2000));
		leilao.propoe(new Lance(usuario, 3000));
		
		assertEquals(1, leilao.getLances().size());
		assertEquals(2000, leilao.getLances().get(0).getValor());
	}
	
	@Test
	public void naoDeveAceitarMaisque5LancesDoMesmoUsuario() {
		Leilao leilao = new Leilao("Macbook Pro 15");
		Usuario jobs = new Usuario("Steve Jobs");
		Usuario gates = new Usuario("Bill Gates");
		
		leilao.propoe(new Lance(jobs, 2000.0));
		leilao.propoe(new Lance(gates, 3000.0));

		leilao.propoe(new Lance(jobs, 2500.0));
		leilao.propoe(new Lance(gates, 3070.0));
		
		leilao.propoe(new Lance(jobs, 200.0));
		leilao.propoe(new Lance(gates, 2300.0));
		
		leilao.propoe(new Lance(jobs, 52.0));
		leilao.propoe(new Lance(gates, 3230.0));
		
		leilao.propoe(new Lance(jobs, 2005.0));
		leilao.propoe(new Lance(gates, 9000.0));

		leilao.propoe(new Lance(jobs, 999.0));
		
		assertEquals(10, leilao.getLances().size());
		assertEquals(9000.0, leilao.ultimoLanceDado().getValor());
	}
	
	@Test
	public void testaDobroDoUltimoLance() {
		Leilao leilao = new Leilao("Macbook Pro 15");
		Usuario jobs = new Usuario("Steve Jobs");
		Usuario gates = new Usuario("Bill Gates");
		
		leilao.propoe(new Lance(jobs, 2000.0));
		leilao.propoe(new Lance(gates, 3000.0));
		
		leilao.dobraLance(jobs);
		
		assertEquals(3, leilao.getLances().size());
		assertEquals(2000.0, leilao.getLances().get(0).getValor());
		assertEquals(3000.0, leilao.getLances().get(1).getValor());
		assertEquals(4000.0, leilao.getLances().get(2).getValor());
		assertEquals(jobs, leilao.getLances().get(2).getUsuario());
	}
	
	@Test
	public void testaDobroDoUltimoLanceDoUsuarioSemLances() {
		Leilao leilao = new Leilao("Macbook Pro 15");
		Usuario jobs = new Usuario("Steve Jobs");
		Usuario gates = new Usuario("Bill Gates");
		Usuario zuckerberg = new Usuario("Mark Zuckerberg");
		
		leilao.propoe(new Lance(jobs, 2000.0));
		leilao.propoe(new Lance(gates, 3000.0));
		
		leilao.dobraLance(zuckerberg);
		
		assertEquals(2, leilao.getLances().size());
		assertEquals(2000.0, leilao.getLances().get(0).getValor());
		assertEquals(3000.0, leilao.getLances().get(1).getValor());
		assertEquals(gates, leilao.getLances().get(1).getUsuario());
	}
}
