package br.com.caelum.leilao.servico;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.caelum.leilao.construtor.CriadorDeLeilao;
import br.com.caelum.leilao.dominio.Lance;
import br.com.caelum.leilao.dominio.Usuario;

public class TesteDoAvaliador {
	private static final String USUARIO_JOSE = "José";
	private static final String USUARIO_MARIA = "Maria";
	private static final String USUARIO_JOAO = "João";
	private static final String PRODUTO = "Playstation 3 novinho em folha";

	Avaliador leiloeiro;
	Usuario joao;
	Usuario jose;
	Usuario maria;
	
	@Before
	public void setUp() {
		leiloeiro = new Avaliador();
		joao = new Usuario(USUARIO_JOAO);
		jose = new Usuario(USUARIO_JOSE);
		maria = new Usuario(USUARIO_MARIA);
	}
	
	@Test(expected=RuntimeException.class)
	public void naoDeveAvaliarLeiloesSemLance() {
		leiloeiro.avalia(new CriadorDeLeilao(PRODUTO).constroi());
	}
	
	@Test
	public void deveEntenderLancesEmOrdemCrescente() {
		leiloeiro.avalia(new CriadorDeLeilao(PRODUTO)
				.lance(joao, 250.0)
				.lance(jose, 300.0)
				.lance(maria, 400.0)
				.constroi());
		assertEquals(400, leiloeiro.getMaiorLance(), 0.00001);
		assertEquals(250, leiloeiro.getMenorLance(), 0.00001);
	}
	
	@Test
	public void deveEntenderLancesEmOrdemDecrescente() {
		leiloeiro.avalia(new CriadorDeLeilao(PRODUTO)
				.lance(joao, 400.0)
				.lance(jose, 300.0)
				.lance(maria, 200.0)
				.lance(joao, 100.0)
				.constroi());
		assertEquals(400, leiloeiro.getMaiorLance(), 0.00001);
		assertEquals(100, leiloeiro.getMenorLance(), 0.00001);
	}

	@Test
	public void deveEntenderLancesEmOrdemAleatoria() {
		leiloeiro.avalia(new CriadorDeLeilao(PRODUTO)
			.lance(joao, 200.0)
			.lance(jose, 450.0)
			.lance(maria, 120.0)
			.lance(joao, 700.0)
			.lance(jose, 630.0)
			.lance(maria, 230.0)
			.constroi());
		assertEquals(700, leiloeiro.getMaiorLance(), 0.00001);
		assertEquals(120, leiloeiro.getMenorLance(), 0.00001);
	}

	@Test
	public void deveCalcularAMedia() {
		leiloeiro.avalia(new CriadorDeLeilao(PRODUTO)
				.lance(joao, 250.0)
				.lance(jose, 300.0)
				.lance(maria, 400.0)
				.constroi());
		assertEquals(316.6666666, leiloeiro.getMediaDosLances(), 0.00001);
	}
	
	@Test
	public void deveEntenderLeilaoComApenasUmLance() {
		leiloeiro.avalia(new CriadorDeLeilao(PRODUTO)
				.lance(joao, 250.0)
				.constroi());
		assertEquals(250, leiloeiro.getMaiorLance(), 0.00001);
		assertEquals(250, leiloeiro.getMenorLance(), 0.00001);
	}
	
	@Test
	public void deveEncontrarOsTresMaiores() {
		// Testa com vários lances
		leiloeiro.avalia(new CriadorDeLeilao(PRODUTO)
				.lance(joao, 100.0)
				.lance(maria, 200.0)
				.lance(joao, 300.0)
				.lance(maria, 400.0)
				.lance(maria, 50.0)
				.constroi());
		List<Lance> maiores = leiloeiro.getTresMaiores();
		assertEquals(3, maiores.size());
		assertEquals(400, maiores.get(0).getValor(), 0.00001);
		assertEquals(300, maiores.get(1).getValor(), 0.00001);
		assertEquals(200, maiores.get(2).getValor(), 0.00001);

		// Testa com apenas dois lances
		leiloeiro.avalia(new CriadorDeLeilao(PRODUTO)
				.lance(joao, 100.0)
				.lance(jose, 300.0)
				.constroi());
		maiores = leiloeiro.getTresMaiores();
		assertEquals(2, maiores.size());
		assertEquals(300, maiores.get(0).getValor(), 0.00001);
		assertEquals(100, maiores.get(1).getValor(), 0.00001);
	}
}
