package br.com.caelum.exercicios;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

public class TesteDaMatematica {

	@Test
	public void testaContaMaluca() {
		MatematicaMaluca matematica = new MatematicaMaluca();
		assertEquals(0, matematica.contaMaluca(0));
		assertEquals(-2, matematica.contaMaluca(-1));
		assertEquals(18, matematica.contaMaluca(9));
		assertEquals(20, matematica.contaMaluca(10));
		assertEquals(33, matematica.contaMaluca(11));
		assertEquals(90, matematica.contaMaluca(30));
		assertEquals(124, matematica.contaMaluca(31));
	}
}
