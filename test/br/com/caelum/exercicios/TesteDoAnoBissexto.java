package br.com.caelum.exercicios;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

public class TesteDoAnoBissexto {

	@Test
	public void testaAnoBissexto() {
		AnoBissexto bissexto = new AnoBissexto();
		assertTrue(bissexto.ehBissexto(2000));
		assertTrue(bissexto.ehBissexto(2004));
		assertFalse(bissexto.ehBissexto(1900));
		assertFalse(bissexto.ehBissexto(2005));
		assertFalse(bissexto.ehBissexto(1));
	}
}
