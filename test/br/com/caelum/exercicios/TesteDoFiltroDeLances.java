package br.com.caelum.exercicios;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import br.com.caelum.leilao.dominio.Lance;
import br.com.caelum.leilao.dominio.Usuario;

public class TesteDoFiltroDeLances {

    @Test
    public void deveSelecionarLancesEntre1000E3000() {
        Usuario joao = new Usuario("Joao");

        FiltroDeLances filtro = new FiltroDeLances();
        List<Lance> resultado = filtro.filtra(Arrays.asList(
                new Lance(joao,2000), 
                new Lance(joao,1000), 
                new Lance(joao,3000), 
                new Lance(joao, 800)));

        assertEquals(1, resultado.size());
        assertEquals(2000, resultado.get(0).getValor(), 0.00001);
    }

    @Test
    public void deveSelecionarLancesEntre500E700() {
        Usuario joao = new Usuario("Joao");

        FiltroDeLances filtro = new FiltroDeLances();
        List<Lance> resultado = filtro.filtra(Arrays.asList(
                new Lance(joao,600), 
                new Lance(joao,500), 
                new Lance(joao,700), 
                new Lance(joao, 800)));

        assertEquals(1, resultado.size());
        assertEquals(600, resultado.get(0).getValor(), 0.00001);
    }
    
    @Test
    public void naoDeveSelecionarLancesEntre700E1000Inclusive() {
    	Usuario joao = new Usuario("Joao");
    	
    	FiltroDeLances filtro = new FiltroDeLances();
    	List<Lance> resultado = filtro.filtra(Arrays.asList(
    			new Lance(joao, 699), 
    			new Lance(joao, 700), 
    			new Lance(joao, 750), 
    			new Lance(joao, 1000),
    			new Lance(joao, 1001)));
    	
    	assertEquals(2, resultado.size());
    	assertEquals(699, resultado.get(0).getValor(), 0.00001);
    	assertEquals(1001, resultado.get(1).getValor(), 0.00001);
    }
    
    @Test
    public void naoDeveSelecionarLancesEntre3000E5000Inclusive() {
    	Usuario joao = new Usuario("Joao");
    	
    	FiltroDeLances filtro = new FiltroDeLances();
    	List<Lance> resultado = filtro.filtra(Arrays.asList(
    			new Lance(joao, 2999), 
    			new Lance(joao, 3000), 
    			new Lance(joao, 4500), 
    			new Lance(joao, 5000),
    			new Lance(joao, 5001)));
    	
    	assertEquals(2, resultado.size());
    	assertEquals(2999, resultado.get(0).getValor(), 0.00001);
    	assertEquals(5001, resultado.get(1).getValor(), 0.00001);
    }
    
    @Test
    public void naoDeveSelecionarLancesMenoresOuIguaisA500() {
    	Usuario joao = new Usuario("Joao");
    	
    	FiltroDeLances filtro = new FiltroDeLances();
    	List<Lance> resultado = filtro.filtra(Arrays.asList(
    			new Lance(joao, 499), 
    			new Lance(joao, 500), 
    			new Lance(joao, 501), 
    			new Lance(joao, 0),
    			new Lance(joao, -1)));
    	
    	assertEquals(1, resultado.size());
    	assertEquals(501, resultado.get(0).getValor(), 0.00001);
    }
    
    @Test
    public void deveSelecionarLancesMaioresQue5000() {
    	Usuario joao = new Usuario("Joao");
    	
    	FiltroDeLances filtro = new FiltroDeLances();
    	List<Lance> resultado = filtro.filtra(Arrays.asList(
    			new Lance(joao, 5000), 
    			new Lance(joao, 5001)));
    	
    	assertEquals(1, resultado.size());
    	assertEquals(5001, resultado.get(0).getValor(), 0.00001);
    }
}
